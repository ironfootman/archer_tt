Daily Planet
==============

REST APIs app

#Installation
--------------
Create virtualenv

mkvirtualenv env -p python3.5

instal dev utils into virtual environment

pip install -r req.txt


#Running
--------------
Migrate database

./manage.py migrate

Load fixtures

./manage.py loaddata daily_planet/fixtures/initial.json

Run application

./manage.py runserver

##URL desctiption:
---------------

##Create user

Method

POST /daily_planet/v1/register/

Parameters

Header parameters:

 NAME  | Is Mandatory? | Description  
------------ | ------------- | -------
Content-Type |  Yes  | Data exchange takes place by means of json messaging

Body parameters:

 NAME  | Is Mandatory? | Description  
------------ | ------------- | -------
    email   |      Yes      |One of required params for register 
  password  |      Yes      |One of required params               
    group   |      Yes      |One of required params              

Response Codes:

  Code   |                      Description                      
-------- | ------------------------------------------------------
   201   | Successful code, token is contained in a response     
   422   | Group is required field                               
   422   | Group with name {name} does not exist                 
   400   | User with that email already exist                    

Request example

```json
{
	"email":"polo1@ua.fm",
	"password": "hello",
	"group": "editor"
}
```
Response example

```json
{
 	"token": "d8ebee0135d338caa1511ff6a68a70aa4eb3c5fa"
}
```
##Login user

Method

POST /daily_planet/v1/login

Parameters

Header parameters:

 NAME  | Is Mandatory? | Description  
------------ | ------------- | -------
Content-Type|      Yes      |Data exchange takes place by means of json messaging


Body parameters:

 NAME  | Is Mandatory? | Description  
------------ | ------------- | -------
    email   |      Yes      |One of required params for register 
  password  |      Yes      |One of required params              


Response Codes:

  Code   |                      Description                      
---------|-------------------------------------------------------
   201   | Successful code, token is contained in a response     
   400   | Unable to log in with provided credentials            

Request example

```json
{
	"email":"polo1@ua.fm",
	"password": "hello",
}
```
Response example

```json
{
 	"token": "d8ebee0135d338caa1511ff6a68a70aa4eb3c5fa"
}
```

##Create post

Method

POST /daily_planet/v1/posts

Parameters

Header parameters:

 NAME  | Is Mandatory? | Description  
------------ | ------------- | -------
Content-Type |      Yes      |Data exchange takes place by means of json messaging
Authorization|      Yes      |Token is required for that operation

Boady parameters:

 NAME  | Is Mandatory? | Description  
------------ | ------------- | -------
    title   |      Yes      |One of required params for posting  |
    body    |      Yes      |One of required params              |

Response Codes:

  Code   |                      Description                      
---------|-------------------------------------------------------
   201   |           Post has been created successfuly           
   400   |            {field_name} is required field             

Request example

```json
{
	"title": "Kyklasdas",
	"body": "HUqwewqeRA"
}
```
Response example

```json
{
  "id": 4,
  "title": "Kyklasdas",
  "body": "HUqwewqeRA",
  "is_published": false
}
```
##Publish post

Method

PUT /daily_planet/v1/posts/publish/{id}

Parameters

Header parameters:

 NAME  | Is Mandatory? | Description  
------------ | ------------- | -------
Content-Type |      Yes      | Data exchange takes place by means of json messaging
Authorization|      Yes      |Token is required for that operation

Boady parameters:

 NAME  | Is Mandatory? | Description  
------------ | ------------- | -------
    title   |      Yes      |One of required params for posting  
    body    |      Yes      |One of required params              

Response Codes:

  Code   |                      Description                      |
---------|-------------------------------------------------------|
   201   |           Post has been created successfuly           
   400   |            {field_name} is required field             
   403   |                        Invalid token                  
   404   |                        Not found                      

Request example

```json
{
	"title": "Kyklasdas",
	"body": "HUqwewqeRA",
	"is_published": false
}
```
Response example

```json
{
  "id": 5,
  "title": "Kykl12s",
  "body": "HUqwewqeRA",
  "is_published": true
}
```
##Profile details

Method

GET /daily_planet/v1/profile

Parameters

Response Codes:

  Code   |                      Description                      
---------|-------------------------------------------------------
   200   |                          OK                           

Request example

/daily_planet/v1/profile

Response example

```json
{
  "id": 2,
  "email": "polo@ua.fm"
}
```

##Get all posts

GET /deily_planet/v1/posts?size={size}&page_size={page_size}

Parameters

Response Codes:

  Code   |                      Description                      |
---------|-------------------------------------------------------|
   200   |                          OK                           

Response example

```json
	{
	  "count": 7,
	  "next": "http://localhost:8000/daily_planet/v1/posts?page=2&page_size=5&size=4",
	  "previous": "http://localhost:8000/daily_planet/v1/posts?page=1&page_size=5&size=4",
	  "results": [
	    {
	      "id": 1,
	      "title": "karamba",
	      "body": "HURA",
	      "is_published": false
	    },
	    {
	      "id": 2,
	      "title": "Kykl12s",
	      "body": "HUqwewqeRA",
	      "is_published": true
	    },
	    {
	      "id": 3,
	      "title": "ka!!1ba",
	      "body": "HUqwewqeRA",
	      "is_published": true
	    },
	    {
	      "id": 4,
	      "title": "Kyklasdas",
	      "body": "HUqwewqeRA",
	      "is_published": false
	    },
	    {
	      "id": 5,
	      "title": "Kykl12s",
	      "body": "HUqwewqeRA",
	      "is_published": true
	    }
	  ]
}
```

