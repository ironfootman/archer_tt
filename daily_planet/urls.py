from django.conf.urls import url
from daily_planet.views import (
    CreateUserView,
    LoginView,
    PostListView,
    PublishedPostView,
    PublishedPostListView,
    CurrentUserView)

import daily_planet.signals

urlpatterns = [
    url(r'^register', CreateUserView.as_view()),
    url(r'^login', LoginView.as_view()),
    url(r'^profile', CurrentUserView.as_view()),
    url(r'^posts$', PostListView.as_view()),
    url(r'^posts/publish/(?P<pk>[0-9]+)', PublishedPostView.as_view()),
    url(r'^published_posts', PublishedPostListView.as_view()),
]
