from django.apps import AppConfig


class DailyPlanetConfig(AppConfig):
    name = 'daily_planet'
