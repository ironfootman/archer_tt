from rest_framework.permissions import BasePermission
from rest_framework.compat import is_authenticated
from rest_framework.filters import SearchFilter
from rest_framework.pagination import PageNumberPagination

from daily_planet.serializers import PostSerializer


class IsEditor(BasePermission):
    def has_permission(self, request, view):
        is_auth = request.user and is_authenticated(request.user)
        return is_auth and request.user.has_perm('daily_planet.status_change')


class Pagination(PageNumberPagination):
    page_size = 20
    page_size_query_param = 'page_size'
    max_page_size = 1000


class PostView:
    filter_backends = (SearchFilter,)
    search_fields = ('title', 'body')
    serializer_class = PostSerializer
    pagination_class = Pagination
